class UpdateColumnLevelToUser < ActiveRecord::Migration
  def change
  	remove_column :Users, :level_permission, :integer
  	add_column :Users, :level_permission, :integer, :default => "1"
  end
end
