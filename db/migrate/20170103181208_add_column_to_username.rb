class AddColumnToUsername < ActiveRecord::Migration
  def change
  	add_column :Users, :name, :string
  	add_column :Users, :last_name, :string
  	add_column :Users, :age, :string
  	add_column :Users, :level_permission, :integer
  end
end
